package com.example.TrainStation.repo;

import com.example.TrainStation.models.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
