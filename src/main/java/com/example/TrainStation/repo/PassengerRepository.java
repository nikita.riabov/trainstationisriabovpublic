package com.example.TrainStation.repo;

import com.example.TrainStation.models.Employee;
import com.example.TrainStation.models.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
}

