package com.example.TrainStation.controllers;

import com.example.TrainStation.models.Employee;
import com.example.TrainStation.models.Passenger;
import com.example.TrainStation.repo.EmployeeRepository;
import com.example.TrainStation.repo.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private PassengerRepository passengerRepository;

    @GetMapping("/")
    public String emp(Model model) {
        List<Passenger> posts = passengerRepository.findAll();
        model.addAttribute("posts",posts);
        return "index";
    }

    @PostMapping("/add")
    public String postEmployee(@RequestParam String fullName, @RequestParam String gender, @RequestParam String phoneNumber, @RequestParam String login, @RequestParam String  password,  Model model){
        Employee employee = new Employee(fullName, gender, phoneNumber, login, password);
        employeeRepository.save(employee);
        return "";
    }

}